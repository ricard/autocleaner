#Boa:Frame:AutoCleanerFrame

import wx, os, sys, fnmatch
import wx.lib.dialogs
import xml.parsers.expat

def create(parent):
    return AutoCleanerFrame(parent)

def FormatSize(size):
    if size < 1024:
        return '%d bytes' % size
    elif size < 1024*1024:
        return '%.1f Kb' % float(size/1024.0)
    else:
        return '%.1f Mb' % float(size/(1024.0*1024))

[wxID_AUTOCLEANERFRAME, wxID_AUTOCLEANERFRAMEBROWSEBUTTON,
 wxID_AUTOCLEANERFRAMECLEAN, wxID_AUTOCLEANERFRAMECLOSE,
 wxID_AUTOCLEANERFRAMENOTEBOOK, wxID_AUTOCLEANERFRAMEPANEL1,
 wxID_AUTOCLEANERFRAMETARGETPATH,
] = [wx.NewId() for _init_ctrls in range(7)]

class AutoCleanerFrame(wx.Frame):
    def _init_coll_topLevelSizer_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.pathSizer, 0, border=10, flag=wx.GROW | wx.ALL)
        parent.AddWindow(self.notebook, 1, border=10, flag=wx.ALL | wx.GROW)
        parent.AddSizer(self.buttonSizer, 0, border=10, flag=wx.GROW | wx.ALL)

    def _init_coll_buttonSizer_Items(self, parent):
        # generated method, don't edit

        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0, proportion=1)
        parent.AddWindow(self.clean, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0, proportion=1)
        parent.AddWindow(self.close, 0, border=0, flag=wx.ALIGN_RIGHT)

    def _init_coll_pathSizer_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.targetPath, 1, border=10,
              flag=wx.ALIGN_CENTER_VERTICAL | wx.RIGHT)
        parent.AddWindow(self.browseButton, 0, border=10,
              flag=wx.LEFT | wx.ALIGN_CENTER)

    def _init_sizers(self):
        # generated method, don't edit
        self.pathSizer = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.topLevelSizer = wx.BoxSizer(orient=wx.VERTICAL)

        self.buttonSizer = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_pathSizer_Items(self.pathSizer)
        self._init_coll_topLevelSizer_Items(self.topLevelSizer)
        self._init_coll_buttonSizer_Items(self.buttonSizer)

        self.panel1.SetSizer(self.topLevelSizer)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Frame.__init__(self, id=wxID_AUTOCLEANERFRAME,
              name=u'AutoCleanerFrame', parent=prnt, pos=wx.Point(550, 357),
              size=wx.Size(547, 456), style=wx.DEFAULT_FRAME_STYLE,
              title=u'AutoCleaner - SoftArchi')
        self.SetClientSize(wx.Size(539, 422))
        self.Center(wx.BOTH)

        self.panel1 = wx.Panel(id=wxID_AUTOCLEANERFRAMEPANEL1, name='panel1',
              parent=self, pos=wx.Point(0, 0), size=wx.Size(539, 422),
              style=wx.TAB_TRAVERSAL)

        self.targetPath = wx.ComboBox(choices=[],
              id=wxID_AUTOCLEANERFRAMETARGETPATH, name=u'targetPath',
              parent=self.panel1, pos=wx.Point(10, 11), size=wx.Size(424, 21),
              style=0, value=u'Entrez un chemin valide...')
        self.targetPath.SetLabel(u'Entrez un chemin valide...')

        self.browseButton = wx.Button(id=wxID_AUTOCLEANERFRAMEBROWSEBUTTON,
              label=u'Browse', name=u'browseButton', parent=self.panel1,
              pos=wx.Point(454, 10), size=wx.Size(75, 23), style=0)
        self.browseButton.Bind(wx.EVT_BUTTON, self.OnBrowseButtonButton,
              id=wxID_AUTOCLEANERFRAMEBROWSEBUTTON)

        self.notebook = wx.Notebook(id=wxID_AUTOCLEANERFRAMENOTEBOOK,
              name=u'notebook', parent=self.panel1, pos=wx.Point(10, 53),
              size=wx.Size(519, 316), style=0)

        self.close = wx.Button(id=wxID_AUTOCLEANERFRAMECLOSE, label=u'Close',
              name=u'close', parent=self.panel1, pos=wx.Point(454, 389),
              size=wx.Size(75, 23), style=0)
        self.close.Bind(wx.EVT_BUTTON, self.OnCloseButton,
              id=wxID_AUTOCLEANERFRAMECLOSE)

        self.clean = wx.Button(id=wxID_AUTOCLEANERFRAMECLEAN, label=u'Clean',
              name=u'clean', parent=self.panel1, pos=wx.Point(160, 389),
              size=wx.Size(144, 23), style=0)
        self.clean.Bind(wx.EVT_BUTTON, self.OnCleanButton,
              id=wxID_AUTOCLEANERFRAMECLEAN)

        self._init_sizers()

    def __init__(self, parent):
        self.config = wx.ConfigBase_Get()
        self._init_ctrls(parent)
##        self.filters = {}    #dictionnary {id: 'filter',...}
        self.sections = {}   #dictionnary {id_button: (id_filter1, id_filter2,...), ...}
        self.curSectionFilters = []   #list (id_filter1, id_filter2,...)
        self.curPanel = None
        self.curPanelSizer = None
        self.curSectionSizer = None
        self.curFilterHelp = None
        parser = xml.parsers.expat.ParserCreate()
        parser.StartElementHandler = self.start_element
        parser.EndElementHandler = self.end_element
        parser.CharacterDataHandler = self.char_data
        parser.ParseFile(open('AutoClean.xml', 'rt'))

        # Loading file history
        self.filehistory = wx.FileHistory()
        self.filehistory.Load(self.config)
        for i in range(self.filehistory.GetCount()):
            file = self.filehistory.GetHistoryFile(i)
            self.targetPath.Append(file)
        if self.targetPath.GetCount() > 0:
            self.targetPath.Select(0)

        self.SetSize(wx.Size(800, 600))

    # 3 handler functions
    def start_element(self, name, attrs):
##        print 'Start element:', name, attrs
        if name == 'panel':
            self.curPanel = wx.Panel(id=wx.NewId(),
                name=attrs['name'], parent=self.notebook, pos=wx.Point(0, 0),
                size=wx.Size(472, 262), style=wx.TAB_TRAVERSAL)
            self.curPanel.filters = {}    #dictionnary {id: 'filter',...}
            self.notebook.AddPage(imageId=-1, page=self.curPanel, select=True, text=attrs['name'])
            self.curPanelSizer = wx.BoxSizer(orient=wx.VERTICAL)

        elif name == 'section':
            staticBox = wx.StaticBox(id=wx.NewId(),
                label=attrs['name'], name=attrs['name'], parent=self.curPanel, style=0)
            self.curSectionSizer = wx.StaticBoxSizer(box=staticBox, orient=wx.HORIZONTAL)
            self.curSectionFilters = []

        elif name == 'filter':
            if attrs.has_key('help'):
                self.curFilterHelp = attrs['help']
            else:
                self.curFilterHelp = ''


    def end_element(self, name):
##        print 'End element:', name
        if name == 'filter':
            self.curFilterHelp = None

        elif name == 'section':
            id = wx.NewId()
            button = wx.Button(id=id,
                  label=u'All / Any', name=u'all',
                  parent=self.curPanel, size=wx.Size(75, 23), style=0)
            button.Bind(wx.EVT_BUTTON, self.OnAllAnyButton,
                  id=id)
            self.curSectionSizer.AddWindow(button, 0, border=5, flag=wx.ALL | wx.ALIGN_CENTER)
            self.sections[id] = self.curSectionFilters
            self.curSectionFilters = []
            self.curPanelSizer.AddSizer(self.curSectionSizer, 0, border=10, flag=wx.ALL | wx.GROW)
            self.curSectionSizer = None

        elif name == 'panel':
            self.curPanel.Layout()
            self.curPanel.SetSizer(self.curPanelSizer)
            self.curPanel = None
            self.curPanelSizer = None

    def char_data(self, data):
        if self.curFilterHelp != None:
##            print 'Character data:', repr(data)
            filter = data.strip()
            if len(filter):
                id = wx.NewId()
                checkBox = wx.CheckBox(id=id, label=data, name=data, parent=self.curPanel)
                checkBox.SetToolTipString(self.curFilterHelp)
                checkBox.SetValue(True)
                self.curSectionSizer.AddWindow(checkBox, 0, border=5, flag=wx.ALL | wx.ALIGN_CENTER)
                self.curPanel.filters[id] = data
                self.curSectionFilters.append(id)

    def OnAllAnyButton(self, event):
        buttonId = event.GetId()
        state = self.FindWindowById(self.sections[buttonId][0]).IsChecked()
        for id in self.sections[buttonId]:
            self.FindWindowById(id).SetValue(not state)

    def OnBrowseButtonButton(self, event):
        path = self.targetPath.GetValue()
        dlg = wx.DirDialog(self, "Choose a path", path)
        if dlg.ShowModal() == wx.ID_OK:
            path = dlg.GetPath()
            i = self.targetPath.FindString(path)
            if i >= 0:
                self.targetPath.Delete(i)
            self.targetPath.Insert(path, 0)
            self.targetPath.Select(0)

            # add it to the history
            self.filehistory.AddFileToHistory(path)
            self.filehistory.Save(self.config)

        dlg.Destroy()

    def OnCloseButton(self, event):
        self.Close()

    def OnCleanButton(self, event):
        path = self.targetPath.GetValue()
        if not os.path.exists(path):
            wx.MessageBox('The path [%s] doesn\'t exist.' % path, 'Path not found', wx.ICON_ERROR)
            return
        i = self.targetPath.FindString(path)
        if i >= 0:
            self.targetPath.Delete(i)
        self.targetPath.Insert(path, 0)
        self.targetPath.Select(0)
        # add it to the history
        self.filehistory.AddFileToHistory(path)
        self.filehistory.Save(self.config)

        filters = []
        panel = self.notebook.GetCurrentPage()
        for id in panel.filters:
            checkBox = self.FindWindowById(id)
            if checkBox.GetValue():
                if panel.filters[id] not in filters:
                    filters.append(panel.filters[id])

        filesToDelete = []
        filesToDeleteStr = ''
        totalSize = 0
        dlg = FileListDialog(self, "Files to delete", 'These following files will be deleted:',
                                size = wx.Size(600, 400), style = wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER);
        for root, dirs, files in os.walk(path):
            for file in files:
                for filter in filters:
                    if fnmatch.fnmatch(file, filter):
                        fname = os.path.join(root, file)
##                        print fname
                        size = os.path.getsize(fname)
                        filesToDelete.append(fname)
                        dlg.Append(fname, isFolder = False, size = size)
##                        filesToDeleteStr += fname + '\t\t[%d]\n' % size
                        totalSize += size
##        dlg.Append('')
##        dlg.Append('Size of files:', size = totalSize)
##        filesToDeleteStr += '\n\nSize of files: ' + FormatSize(totalSize)
##        dlg = wx.lib.dialogs.ScrolledMessageDialog(self, filesToDeleteStr, "Files to delete",
##                                size = wx.Size(600, 400), style = wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER);
        if dlg.ShowModal() == wx.ID_OK:
            i = 0
            max = len(filesToDelete)
            progress = wx.ProgressDialog("Deleting files",
                               "Preparing to delete files",
                               maximum = max,
                               parent=self,
                               style = wx.PD_CAN_ABORT
                                | wx.PD_APP_MODAL
                                | wx.PD_ELAPSED_TIME
                                #| wx.PD_ESTIMATED_TIME
                                | wx.PD_REMAINING_TIME
                                | wx.PD_SMOOTH
                                | wx.PD_AUTO_HIDE
                                )

            keepGoing = True
            count = 0
            try:
                for file in filesToDelete:
                    count += 1
                    keepGoing = progress.Update(count, 'Deleting %s...' % os.path.split(file)[1])
                    if not keepGoing:
                        break
                    try:
                        os.remove(file)
                    except:
                        wx.LogWarning('Can\'t remove file %s' % file)
            finally:
                progress.Destroy()

class FileListDialog(wx.Dialog):
    def __init__(self, parent, caption, msg,
                 pos=wx.DefaultPosition, size=(500,300),
                 style=wx.DEFAULT_DIALOG_STYLE):
        wx.Dialog.__init__(self, parent, -1, caption, pos, size, style)
        x, y = pos
        if x == -1 and y == -1:
            self.CenterOnScreen(wx.BOTH)

        imgList = wx.ImageList(16, 16)
        imgList.Add(wx.ArtProvider_GetBitmap(wx.ART_NORMAL_FILE, wx.ART_OTHER, (16,16)))
        imgList.Add(wx.ArtProvider_GetBitmap(wx.ART_FOLDER, wx.ART_OTHER, (16,16)))


        text = wx.StaticText(self, -1, msg)
        self.listCtrl = wx.ListView(self, -1, size = wx.Size(500, 400))
        self.listCtrl.AssignImageList(imgList, wx.IMAGE_LIST_SMALL)
        self.listCtrl.InsertColumn(0, 'File', width = 400)
        self.listCtrl.InsertColumn(1, 'Size', width = 80)
        ok = wx.Button(self, wx.ID_OK, "OK")
        ok.SetDefault()
        cancel = wx.Button(self, wx.ID_CANCEL, "Cancel")
        buttonsSizer = wx.BoxSizer(wx.HORIZONTAL)
        buttonsSizer.AddSpacer((1,1), 1)
        buttonsSizer.AddWindow(ok, 0, wx.ALL, 10)
        buttonsSizer.AddSpacer((30, 1))
        buttonsSizer.AddWindow(cancel, 0, wx.ALL, 10)
        buttonsSizer.AddSpacer((1,1), 1)

        totalSizer = wx.BoxSizer(wx.HORIZONTAL)
        totalSizer.AddWindow(wx.StaticText(self, -1, 'Size of files:'), 0, wx.ALIGN_CENTER | wx.ALL, 10)
        self.sizeCtrl = wx.TextCtrl(self, -1, size = wx.Size(80, -1))
        totalSizer.AddWindow(self.sizeCtrl, 0, wx.ALL, 10)

        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.AddWindow(text, 0, wx.ALL, 10)
        sizer.AddWindow(self.listCtrl, 1, wx.GROW | wx.ALL, 10)
        sizer.AddSizer(totalSizer)
        sizer.AddSizer(buttonsSizer, 0, wx.GROW)

        self.SetSizer(sizer)
        sizer.SetSizeHints(self)

        self.fullSize = 0

##        lc = layoutf.Layoutf('t=t5#1;b=t5#2;l=l5#1;r=r5#1', (self,ok))
##        text.SetConstraints(lc)
##
##        lc = layoutf.Layoutf('b=b5#1;x%w50#1;w!80;h*', (self,))
##        ok.SetConstraints(lc)
##        self.SetAutoLayout(1)
##        self.Layout()

    def Append(self, file, isFolder = False, size = 0):
        if isFolder:
            image = 1
        else:
            image = 0
##        self.listCtrl.InsertImageStringItem(0, file, image)
        item = self.listCtrl.Append((file, FormatSize(size)))
        self.listCtrl.SetItemImage(item, image)
        self.fullSize += size
        self.sizeCtrl.SetValue(FormatSize(self.fullSize))
