a = Analysis([os.path.join(HOMEPATH,'support\\_mountzlib.py'), os.path.join(HOMEPATH,'support\\useUnicode.py'), 'AutoCleanerApp.py'],
             pathex=['D:\\Develop\\Python\\AutoCleaner'])
#images = Tree('images', 'images', ('.svn',))
pyz = PYZ(a.pure)
exe = EXE(pyz,
          a.scripts + [('O','','OPTION')],
          exclude_binaries=1,
          name='AutoCleaner.exe',
          debug=0,
          strip=0,
          upx=1,
          console=0 , version='version.txt')
coll = COLLECT( exe,
               a.binaries,
               strip=0,
               upx=1,
               name='distin-AutoCleaner')
